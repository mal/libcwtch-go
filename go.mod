module git.openprivacy.ca/flutter/libcwtch-go

go 1.15

require (
	cwtch.im/cwtch v0.8.12
	git.openprivacy.ca/openprivacy/connectivity v1.4.5
	git.openprivacy.ca/openprivacy/log v1.0.2
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
)
